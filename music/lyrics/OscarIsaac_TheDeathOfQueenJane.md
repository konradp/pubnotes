# Oscar Isaac - The death of queen Jane

Queen Jane lay in labor
Full nine days or more
Til her women grew so tired
They could no longer there
They could no longer there

Good women, good women
Good women that ye be
Will you open my right side
And find my baby
And find my baby

Oh, no! cried the women
That's a thing that can never be
We will call on King Henry
To hear what he may say
And hear what he may say

King Henry was sent for
King Henry he did come
Saying what ail you, my lady
Your eyes they look so dim
Your eyes they look so dim

King Henry, King Henry
Will you do one thing for me
Will you open my right side
And find my baby
And find my baby
Oh, no! cried King Henry
That's a thing that I can never do
If I lose the flower of England
I shall lose the branch too
I shall lose the branch too

There was fiddling and dancing
On the day the babe was born
But for Queen Jane, beloved
She laid cold as a stone
Laid cold as a stone
