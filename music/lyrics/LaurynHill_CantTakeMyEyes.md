Comparing Frankie Valli's original against Lauryn Hill's version.

Lauryn Hill - E major
[INTRO]
E
```
[VERSE 1]
I
You're just too good to be true
I
Can't take my eyes off of you
I
You'd be like heaven to touch
IV
I want to hold you so much
IV
At long last love has arrived
III
And I thank God I'm alive
II
You're just too good to be true
I
I can't take my eyes off of you

[VERSE 2]
Pardon the way that I stare
There's nothing else to compare
The sight of you leaves me weak
There're no words left to speak
But if you feel like I feel
Please let me know that it's real
You're just too good to be true
Can't take my eyes off of you

[CHORUS]
I love you baby, and if it's quite all right
I need you baby, to warm a lonely night
I love you baby, trust in me when I say
Oh, pretty baby, don't bring/let me down, I pray
Oh, pretty baby, now that I found you, stay
And let me love you, baby, let me love you

[VERSE 1]
[CHORUS x 3]
