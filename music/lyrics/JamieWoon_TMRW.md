# Jamie Woon - TMRW
# (acoustic)
# https://www.youtube.com/watch?v=TVBDsebrreE
# Note: The album version is bad, see the acoustic from kapelsessie

[INTRO]

[VERSE 1]
All right, let's take it from the top again
Rewrite the pieces that ain't fitting in
Bring it back to the ground
(and) Dust it down and sweep the floor
High time to do what ain't been done before

[INTERLUDE]
Take a breath and the day comes round
Hold it down and keep it calm
If you can for the sake of things
How do we get through tomorrow every day?
(what) I don't know but we get through it anyway
Another day comes round

[VERSE 2]
Right now we're older than we've ever been
Swan out by dreams that could be happening
Know just one thing that ain't
Tomorrow's coming down the line
Today's been dawning since the end of time

[INTERLUDE]
Take a breath and the day comes round
Hold it down and keep it calm
If you can for the sake of things/piece
How do we get through tomorrow every day?
I don't know but we get through it anyway
How do we get through tomorrow every day?
I don't know but we get through it anyway
(and) Another day comes round
