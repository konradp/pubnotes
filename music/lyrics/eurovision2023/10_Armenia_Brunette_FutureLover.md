[VERSE 1]
I just wanna make art, read books and just find someone
Who likes me enough to kiss my face
I wanna explore with him and visit old bookstores
And cute little things, like drink smoothies at near cafés

Oh-oh-oh
Drink smoothies at near cafés
Oh-oh-oh

[VERSE 2]
Oh, future lover, I hope it all comes naturally
I hope our love is quiet outside, but loud inside, oh baby
Oh future lover, this song I wrote for you
This song I wrote for you, my future lover

[CHORUS]
I decide to be good, do good, look good
I decide to be good, do good, look good
I decide to be good, do good, look good
I decide to be good, do good, look good

[RAP BIT]
It's like a daydream, but I got some other, better plans
I wanna scream and shout, my heart caught in chains
Cold heart, cold hands, fire in my veins
Fire in my veins, heart in chains
I'm a volcano that is going to explode in a sec
I'm so hypnotised by someone that I've never ever met
Don't wanna forget, am I dreaming yet?
Poetic dream, I don՚t want it to end, oh

Three minutes of making impossible plans
Seven minutes of unnecessary panic attacks
Here I go with the coldest hands
Here I go with still no plans oh, still no plans
I can't cool off, no I can't relax
Lord, what I'm gonna do, my pain just attacks
I still have the coldest hands
Oh my Lord, my Lord, my pain, my panic attacks, oh

[CHORUS]

Ու դու ինձնից հեռու [You’re so far away from me]
Լքված ես հեռու, հեռու ես դու [Left alone, far, far away]
Երազումս ես ու դու էինք լուռ [You and I, silent in my dream]
Մոլորվում էինք հեռու - հեռու հեռվում [We’re lost, far, far away]
