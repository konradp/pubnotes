[CHORUS]
She, queen of the kings
Runnin' so fast, beatin' the wind
Nothin' in this world can stop the spread of her wings
She, queen of the kings
Broken her cage, threw out the keys
She will be the warrior of North and Southern seas

[VERSE 1]
Got raven hair, it's dark as night
Icy eyes, outta sight, outta sight
Her heart, in spite, is warm and bright
Her smile awakes the northern light

[PRE-CHORUS]
Lookin' out, she calls
Lai, la-la-li, rai-ra
Who will conquer all?!

Her name is
[CHORUS]

[VERSE 2]
A firestone forged in flames
Wildest card, run the game, run the game
Can't say the same in this world of change
Don't fear the pain, just break the chain

[PRE-CHORUS]

Her name is
[CHORUS]

Lai, la-li-la
Rai, rai-ri, rai, rai-ri
Lai, la-la-li-lai, lai-ri-ra
Lai-la-li-la, la, la-la, la
La, la-la, la
Ah! Ah! Ah! Ah!

Her name is
[CHORUS]
