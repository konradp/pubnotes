# Eurovision 2023

# I like
01 France     | La Zarra - Évidemment
02 San Marino | Piqued Jacks - Like An Animal
03 Lithuania  | Monika Linkytė - Stay
04 Latvia     | Sudden Lights - Aijā
05 Poland     | Blanka - Solo
06 UK         | Mae Muller - I Wrote A Song
07 Czechia    | Vesna - My Sister's Crown
08 Sweden     | Loreen - Tattoo
09 Norway     | Alessandra  - Queen of Kings
10 Armenia    | Brunette - Future Lover


# I like, but less
11 Serbia      | Luke Black - Samo Mi Se Spava
12 Denmark     | Reiley - Breaking My Heart
13 Slovenia    | Joker Out - Carpe Diem
14 Romania     | Theodor Andrei - D.G.T. (Off and On)
15 Spain       | Blanca Paloma - Eaea
?? Estonia     | Alika - Bridges
?? Moldova     | Pasha Parfeni - Soarele şi Luna
?? Netherlands | Mia Nicolai & Dion Cooper - Burning Daylight
?? Malta       | The Busker - Dance (Our Own Party)

# So so, but no hate
Albania | Albina & Familja Kelmendi - Duje
Cyprus  | Andrew Lambrou - Break A Broken Heart
Finland | Käärijä - Cha Cha Cha
Greece  | Victor Vernicos - What They Say
Iceland | Diljá - Power
Israel  | Noa Kirel - Unicorn
Italy   | Marco Mengoni - Due Vite

# I don't like, sorry
Australia   | Voyager - Promise
Austria     | Teya & Salena - Who The Hell Is Edgar?
Azerbaijan  | TuralTuranX - Tell Me More
Belgium     | Gustaph - Because Of You
Croatia     | Let 3 - Mama ŠČ!
Georgia     | Iru - Echo
Germany     | Lord of the Lost - Blood & Glitter
Ireland     | Wild Youth - We Are One
Portugal    | Mimicat - Ai Coração
Switzerland | Remo Forrer - Watergun
Ukraine     | TVORCHI - Heart of Steel
