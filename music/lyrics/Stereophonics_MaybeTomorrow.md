# Stereophonics - Maybe Tomorrow

[VERSE 1]
I've been down, and I'm wondering why
These little black clouds keep walking around
With me, with me

It wastes time, and I'd rather be high
Think I'll walk me outside and buy a rainbow smile
But be free - they're all free

[CHORUS]
So maybe tomorrow
I'll find my way home
So maybe tomorrow
I'll find my way home

[VERSE 2]
I look around at a beautiful life
I've been the upper side of down, been the inside of out
But we breathe, We breathe

I want a breeze and an open mind
I wanna swim in the ocean, wanna take my time
For me, all me

[CHORUS]
[SOLO]
[CHORUS]
[CHORUS]
[OUTRO]
