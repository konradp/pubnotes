# John Mayer - Do you know me

[Verse 1]
It's just the strangest thing
I've seen your face somewhere
An early evening dream, a past life love affair

[Chorus]
Do you know me at all?
Do you know me at all?

[Verse 2]
In all my reverie
I thought I felt us there
A feather in my hand
A flower in your hair

[Chorus]
Do you know me at all?
Do you know me at all?
