# Arctic Monkeys - Do I wanna know
tempo: 84
[INTRO]
Bb | Gm | Eb | C | Gm | Gm

[VERSE 1]
Gm                          Eb
Have you got colour in your cheeks?
Gm                   C                   Gm
Do you ever get that fear that you can't shift the tide
    C                   Hm
That sticks around like something in your teeth?
Gm             Eb
Are there some aces up your sleeve?
Gm          C         Gm
Have you no idea that you're in deep?
                 C    Gm
I've dreamt about you nearly every night this week
         Eb
How many secrets can you keep?
C
'Cause there's this tune I found
Gm
That makes me think of you somehow and I play it on repeat
Eb                   C                     D
Until I fall asleep, spilling drinks on my settee


[PRE-CHORUS]
D           Eb             C
(Do I wanna know?) If this feelin' flows both ways?
        Gm      C                                 F
(Sad to see you go) Was sort of hoping that you'd stay
          C    Eb             C
(Baby, we both know) That the nights were mainly made
           Gm                    C            Gm
For sayin' things that you can't say tomorrow day

[CHORUS]
F                Gm
Crawling back to you
                Ebm
Ever thought of calling when
You've had a few?
                Gm
'Cause I always do
Maybe I'm too

           Eb
Busy being yours
            C
To fall for somebody new
                     Gm
Now, I've thought it through (dua lipa ends here)
Crawling back to you

[VERSE 2]
So have you got the guts?
Been wondering if your heart's still open
And if so, I want to know what time it shuts
Simmer down and pucker up, I'm sorry to interrupt
It's just I'm constantly on the cusp of trying to kiss you
But I don't know if you feel the same as I do
But we could be together if you wanted to

[PRE-CHORUS]
[CHORUS]
