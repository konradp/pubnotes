# Selah Sue - Raggamuffin

You never had it easy I know
But I still remember you and what we used to say, so
I say this my song for you my friend
You can only see that I can hardly let things go, no oh yeah
So listen to the sound of my voice, yo brother send him all my love
He’s giving me no choice, no no no
Listen to the sound, yeah the boys:
Raggamuffin is a freedom fighter, he’s handling a choice, and I know that

[CHORUS]
The raggamuffin is one of the friends,
and what you see is what you really need in the end
But what you ever gonna gonna do, I don’t know
The raggamuffin shall not fall down,
‘cause he has the wisdom of not fooling around,
But why then shove your good sense underground?

You never had it easy I know
But I still remember you and what we used to have so
(I say), this my song for you my friend
You can only see that I will never forget this

[SOLO]

[CHORUS]

[OUTRO]
