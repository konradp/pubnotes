# Lullaby
# Eugene Field - te song of luddy-dud
# (from love-songs of childhood)
# music: Don't scratch the mosquitoe bites
'tis little luddy-dud in the morning
'tis little luddy-dud at night
and all day long
'tis the same dear song
of that growing, knowing, knowing little sprite,
luddy-dud

luddy-dud's cradle is singing
where softly the night winds blow
and luddy-dud's mother is singing
a song that is sweet and low

'tis little luddy-dud in the morning
'tis little luddy-dud at night
and all day long
'tis the same sweet song
of my nearest and my dearest heart's delight,
luddy-dud!

----

A sunbeam comes a-creeping
into my dear one's nest
and sings to our babe a-sleeping
the song that I love the best

[CH]
'tis little luddy-dud in the morning
'tis little luddy-dud at night
and all day long
'tis the same sweet song
of that waddling, toddling, coddling little mite
luddy-dud

The bird to the tossing clover
the bee to the swaying bud
keep singing that sweet song over
of wee little luddy-dud
