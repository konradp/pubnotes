# Pixie Lott - Mama Do (Uh Oh, Uh Oh)
# key: Ebm
# chords:
# Ebm Abm
# Bb Ebm
# Em Am
# B Em

[VERSE 1]
Every night I go
Every night I go sneaking out the door
I lie a little more, baby I'm helpless

There's something 'bout the night
And the way it hides all the things I like
Little black butterflies
Deep inside me

[CHORUS]
What would my mama do (Uh Oh Uh Oh)
If she knew 'bout me and you? (Uh Oh Uh Oh)
What would my daddy say (Uh Oh Uh Oh)
If he saw me hurt this way? (Uh Oh Uh Oh)

[VERSE 2]
Why should I feel ashamed?
Feeling guilty at the mention of your name
Here we are again
It's nearly perfect

[CHORUS]

[BRIDGE INTERLUDE]
What would my mama do (Oh Oh)
What would my daddy say (Oh Oh)

[BRIDGE]
All the things a girl should know
Are the things she can't control
All the things a girl should know
She can't control

[CHORUS/OUTRO]
What would my mama do (Uh Oh Uh Oh)
If she knew 'bout me and you (Uh Oh Uh Oh)
What would my daddy say (Uh Oh Uh Oh)
If he saw me hurt this way (Uh Oh Uh Oh)
Uh Oh Uh Oh
Uh Oh Uh Oh
Uh Oh Uh Oh
