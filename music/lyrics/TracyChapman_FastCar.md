# Tracy Chapman - Fast Car
You got a fast car
I want a ticket to anywhere
Maybe we make a deal
Maybe together we can go somewhere

Anyplace is better
Starting from zero, got nothing to lose
Maybe we'll make something
But me myself, I got nothing to prove

You got a fast car
And I got a plan to get us out of here
I been working at a convenience store
Managed to save just a little bit of money

We won't have to drive too far
Just 'cross the border and into the city
You and I can both get jobs
And finally see what it means to be living

You see my old man's got a problem
He live with the bottle, that's the way it is
He says his body's too old for working
I say his body's too young to look like his
My mama went off and left him
She wanted more from life than he could give
I said somebody's got to take care of him
So I quit school and that's what I did
