# The Clancy Brothers - Will Ye Go, Lassie, Go?
# (Wild Mountain Thyme)
# (Purple Heather)

[VERSE 1]
Oh the summertime is coming
And the trees are sweetly blooming
And the wild mountain thyme
Grows/all around the blooming heather
Will ye go, Lassie go?

[CHORUS]
And we'll all go together
To pluck/pull wild mountain thyme
All/from around the blooming/purple heather
Will ye go, Lassie go?

[VERSE 2]
I will build my love a tower/bower
Near/by yo'n pure ('n) crystal fountain
Yes/and on it I will build/pile/lay
All the (wild) flowers of the mountain
Will ye go, Lasse go?

[CHORUS]

[VERSE 3]
If my true love she were gone (won't have me)
I would surely find another
Where wild mountain thyme / To pull wild mountain thyme
Grows around the blooming heather / All around the purple heather
Will ye go, Lassie go?

[CHORUS]
[VERSE 1]
[CHORUS]
