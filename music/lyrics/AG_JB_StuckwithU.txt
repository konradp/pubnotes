I'M the one to stick a*round*
ONE strike and you're *out*, baby
Don't care if I *sound* crazy
BUT you never let me *down*, no, no **
That's why
WHEN the sun's up *I'm* stayin'
STILL layin' in your *bed* singin'

Ooh, ooh, ooh, ooh
*Got* all this TIME on my hands
*Might* as well cancel our *plans*, yeah
I could stay here for a lifetime

So lock the door
And throw out the key
Can't fight this no more
It's just you and me
And there's nothing I, nothing I, I can do
I'm stuck with you, stuck with you, stuck with you
