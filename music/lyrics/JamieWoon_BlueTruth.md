# Jamie Woon - Blue Truth
# (acoustic version from Mahogany Session)
# (https://www.youtube.com/watch?v=4xF7lY1Xj1w)

Come on man resurrect yourself
Tell yourself a thought that's true
Come on man and protect yourself
There's nothing that you can't come through
And I ain't gonna go blindly
Living in an empty shell
There's nothing that you can't come through
There's nothing that you can't come through
Through
Come on man and collect yourself
Sing a song with a different tune
Come on man redirect yourself
There's nothing but the world in view
And I ain't gonna die crying
Breathing from an empty shell
There's nothing that you can't come through
There's nothing that you can't come through
Through
Through
Through
Through
Truth
