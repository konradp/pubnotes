# John Mayer - Everything you'll ever be
# (bathroom song)

[VERSE 1]
Beautiful, I watch you try
To see yourself through others' eyes
But mirrors are a losing game
They only show you backwards anyway

[VERSE 1: CH]
The magic and the misery
Come and go so easily
But everything you'll ever be
You already are to me

[VERSE 2]
And you were only five years old
Playing princess in your mother's clothes
Could you feel me standing next to you
With my plastic sword and playground shoes

[VERSE 2: CH]
Saying If my dear I'm wrong somehow
May dragons come and fell me now
'Cause everything you'll ever be
You already are to me

[BRIDGE]
Why, I don't know why
I don't know why you think you need to do it
How, I don't know how
I don't know how but I just sit back and I see my way right through it

[VERSE 3]
When the memories stand ten feet tall
Casting/throwing shadows on your bedroom wall
When you pull the shades and kill the lights
Will you hear me singing out tonight?
Will you hear me singing out tonight?

[VERSE 3: CH]
Days are long and words are cruel
But they won't get the best of you
'Cause everything you'll ever be
You have always been to me
Always been to me
