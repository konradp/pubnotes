# Arctic Monkeys - Do I wanna know
[VERSE 1]
Have you got colour in your cheeks?
Do you ever get that fear that you can't shift the tide
That sticks around like something in your teeth?
Are there some aces up your sleeve?
Have you no idea that you're in deep?
I've dreamt about you nearly every night this week

How many secrets can you keep?
'Cause there's this tune I found
That makes me think of you somehow and I play it on repeat
Until I fall asleep, spilling drinks on my settee


[PRE-CHORUS]
(Do I wanna know?) If this feelin' flows both ways?
(Sad to see you go) Was sort of hoping that you'd stay
(Baby, we both know) That the nights were mainly made
For sayin' things that you can't say tomorrow day

[CHORUS]
Crawling back to you
Ever thought of calling when
You've had a few?
'Cause I always do
Maybe I'm too

Busy being yours
To fall for somebody new
Now, I've thought it through (dua lipa ends here)
Crawling back to you

[VERSE 2]
So have you got the guts?
Been wondering if your heart's still open
And if so, I want to know what time it shuts
Simmer down and pucker up, I'm sorry to interrupt
It's just I'm constantly on the cusp of trying to kiss you
But I don't know if you feel the same as I do
But we could be together if you wanted to

[PRE-CHORUS]
[CHORUS]
