More convenient than a Tesco Express
Close to Windsor but the propety's less
It keeps the businesses of Britain great
It's got Europe's biggest trading estate

It doesn't matter where you're from
You wanna work? Then come along
The station's just got a new floor
And the motorway runs by your door

And you know just where you're heading
It's equidistance 'tween London and Reading

# Chorus
Oh, oh, oh Slough (Slough)
My kind of town
I don't know how anyone could put you down

[Verse 2: David Brent]
To the west, you got Taplow and Bray
You've got Hillingdon the other way
It's a brilliant place to live and work
It was in Bucks now officially it's Berks

Don't believe what the critics say
Like it's soulless and boring and grey
See for yourself, what are you waiting for?
We're on the Bath road, that's the A4

And you know just where you're heading
It's equidistance 'tween London and Reading

# Choru
Oh Slough (Slough)
My kind of town
I don't know how anyone could put you down

And you know just where you'll be heading
It's equidistance 'tween London and Reading

# Chorus
Oh Slough (Slough)
My kind of town
I don't know how​ anyone could put you down

[x4] Slough
