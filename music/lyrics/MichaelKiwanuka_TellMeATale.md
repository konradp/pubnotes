# Michael Kiwanuka - Tell me a tale

[VERSE 1]
Tell me a tale that always was
Sing me a song that I'll always be in
Tell me a story that I can read
Tell me a story that I believe

[VERSE 2]
Paint me a picture that I can see
Give me a touch that I can feel
Turn me around so I can be
Everything I was meant to be

[CHORUS]
Lord, I need loving
Lord, I need good, good loving
Lord, I need loving
Lord, I need good, good loving

[VERSE 3]
Show me some strength that I can use
Give me a sound that I won't refuse
Tell me story that I can read
Tell me a story that I can believe

[VERSE 4]
Tell me a tale that always was
Sing me a song that I'll always be in
Turn me around so I can be
Everything I was meant to be

[CHORUS]
[CHORUS]

[OUTRO and SOLO] See recording
