# Jamie Woon - Street
# key: E/C#m
# chords:
# Em7 D7
# C7 B7 Em
# F Am7 C B7sus4/B7

[VERSE 1]
Walking and walking
Thinking on my feet
Anything can happen in the city
But you can't sit down

[VERSE 2]
Building to building
Sheltering from the sky
Knowing there's somebody on the street
That could change my life

[CHORUS]
You can try on anything for free
Pick up anything you need
And I'm wishing you were here with me
Walking on a city street

[INTERLUDE]

[VERSE 3]
Window to window
Filling in the hours
Catching my reflection
In a place I've never been before

[VERSE 4]
Crossing the bridges
Leap the river wide
Knowing not in time
I'll arrive on the other side

[CHORUS]
