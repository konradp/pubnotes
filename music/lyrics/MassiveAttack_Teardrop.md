# Massive Attack - Teardrop
4/4
32 steps or 16 steps
Notes:
- second kick is softer
- stick is loud

[INTRO]: (A G D  A G D)
---- | ---- | ---- | ----       just drums here
---- | ---- | ---- | ----

A--- | ---e | A--- | ---a       fade in keys (long note is not heard on first bar here)
A--- | ---e | A--- | ---a
A--- | ---e | A--- | ---a       keys still fading in
A--- | ---e | A--- | ---a

A--- | ---e | A--- | ---a
AAAA | ---e | GGGG | ---e
DDDD | ---e | AAAA | ---e
GGGG | ---e | DDDD | ---e


[VERSE 1]: A G D A | G D D A
Love, love is a verb, Love is a doing word               V1/L1
AAAA | ---e         | GGGG | ---a

Fearless on my        breath                             V1/L2
DDDD | ---e         | AAAA | ---a

Gentle impulsion      Shakes me, makes me lighter        V1/L3
AAAA | ---a         | GGGG | ---e

Fearless on my        breath                             V1/L4
DDDD | ---a         | AAAA | ---a


[CHORUS]: F G A
Teardrop on the fire, Fearless on my                     C1/L1
FFFF | ---a         | GGGG | ---e

breath                                                   C1/L2
AAAA | ---a         | .... | ---a


[INTERLUDE1]:
---- | ---- | ---- | ----       just drums here          I1/L1
---- | ---- | ---- | ----                                I1/L2


[VERSE2]: A G D A | F G A F
Night, night after day, Black flowers blosson            V2/L1
AAAA | ---e           | GGGG | ---a

Fearless on my          breath                           V2/L2
DDDD | ---e           | AAAA | ---a

Black flowers blossom   Fearless on my                   V2/L3
FFFF | ---e           | GGGG | ---a

breath                  ...                              V2/L4
AAAA | ---e           | FFFF | ---e


**TODO**

#### LYRICS ONLY

[V1]: (AGDA | AGDA)

A                     G
Love, love is a verb, Love is a doing word
D                     A
Fearless on my        breath
A                     G
Gentle impulsion      Shakes me, makes me lighter
D                     A
Fearless on my        breath

[CH]: F G A
F                     G
Teardrop on the fire, Fearless on my
A                     ...
breath                ...

[INTERLUDE]: ii ii ii ii

[V2]: (AGDA/FGAF/DGA.)
A                       G
Night, night after day, Black flowers blosson
D                       A     
Fearless on my          breath
F                       G
Black flowers blossom   Fearless on my
A                       F
breath                  ...

[CH]:
D                     G
Teardrop on the fire, Fearless on my
A                     ...
...

[V3]:
Water is my eye
Most faithful mirror
Fearless on my breath
Teardrop n the fire
Of a confession
Fearless on my breath
Most faithful mirror
Fearless on my breath

[CH]:
Teardrop on the fire
Fearless on my breath

[OUTRO]:
You're stumbling in the dark
You're stumbling in the dark
