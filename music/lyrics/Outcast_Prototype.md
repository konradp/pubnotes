# Outkast - Prototype (D-minor)

I hope that you're the one
If not, you are the prototype
We'll tiptoe to the sun
And do things I know you like

[CH]
I think I'm in love, again [x2]

Today must be my lucky day
Baby, you are the prototype
Do something out the ordinary
Like catch a mantee
Baby you are the prototype

[CH]
If we happen to part
Lord knows I don't want that
But hey, we can't be mad at God
We met today for a reason
I think I'm on the right track now

[CH]
The scene [let's go, let's go]
Come here [to the movies, yeah]

[CH]
Girl, right now I want to say, I want to say
For picking me up
And bringing be back to this world
I can't, I'm not
I can't afford to not record
