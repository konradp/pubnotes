# Scale
D (Bm)
# Voice notes
A F#

# Chords
verse 1

D  D  A  A
Bm Bm G  G
D  D  A  A
D  G  D A7

I  I  V  V
vi vi IV IV
I  I  V  V
I  iv I  IV7

verse 2

D  D  A  A
Bm Bm G  G
D  D  A  A
D  G  D  D

I  I  V  V
vi vi IV IV
I  I  V  V
I  iv I  I

chorus
F/C F/C  C  C
Bb  Bb   A  A
F   ?    Gm A
E   E    A  A7



