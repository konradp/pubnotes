\version "2.18.2"
\language "english"
<<
  \new Staff {
    \key c \minor
    \time 4/4
    \relative {
      af' bf c | |
    }
  }
  \new Staff {
    \key c \minor
    \time 4/4
    \relative {
      {<ef' bf'> g } {<ef bf'> g} | {<ef bf'> g } {<ef bf'> g} |
      {<c, g'> ef }  {<c g'> ef } | {<c g'> ef }  {<c g'> ef }
      %\bar "|."
    }
  }
>>

<<
  \new Staff {
    \key c \minor
    \time 4/4
    \relative {
      af' bf c | |
    }
  }
  \new Staff {
    \key c \minor
    \time 4/4
    \relative {
      {<ef' bf'> g } {<ef bf'> g} | {<ef bf'> g } {<ef bf'> g} |
      {<c, g'> ef }  {<c g'> ef } | {<c g'> ef }  {<c g'> ef }
      %\bar "|."
    }
  }
>>