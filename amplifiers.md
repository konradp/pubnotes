# guitar processors
- page: http://mmg-music.com/en/effects/guitar-processors?Filters%5BpriceFrom%5D=0.00&Filters%5BpriceTo%5D=950.00&view=1&search=&product_cat=&order=1&per-page=24
- 130: zoom 3030 guitar processor
- 200: nux mg-200 guitar modelling processor
- 280: boss BE-5 guitar multi-effect
- 290: boss GT-5 guitar processor
- 320: boss BE-5M multi effect
- 330: Korg AX3000G toneworks
- 350: line6 floor pod plus
- 360: nux cerberus multi effect
- 390: digitech jamman looper & delay processor
- 420: boss G-10 guitar processor
- 420: rocktron utopia g300 guitar processor
- 450: vox tonelab le guitar processor
- 450: line6 FM4 filter modeler
- 480: boss SL-20 slicer audio pattern processor
- 490: mooer preamp live


# effects
- page: http://mmg-music.com/en/effects/guitar-effects?Filters%5BpriceFrom%5D=0.00&Filters%5BpriceTo%5D=850.00&view=1&search=&product_cat=&order=1&per-page=24
- 90: ts5 tube screamer
- 100: VMB fuzz box
- 100: VMB TS drive
- 100: VMB plexi bite
- 100: VMB blues box
- 140: ARIA TD-1 platinum drive tube OD
- 180: Mojo gear MKII professional fuzz MP40
- 180: Mojo gear deluxe BC108 silicone fuzz
- 280: Ibanez STL super tube vintage overdrive

# tube amps
- 320: VMB patch amp tube combo
- 390: VMB champ 7 tube combo

# transistor amps
- 220: Ecolette CA30 guitar combo
- 250: Harley Benton HBAC-30 battery combo
- 250: keybone MF-70A multi instruments combo
- wildcat WG50 guitar combo
