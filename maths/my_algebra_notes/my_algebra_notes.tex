\documentclass[12pt]{article}
\usepackage{amsthm, amsmath}
\usepackage[a4paper, margin=0.5cm]{geometry}
\usepackage{hyperref}
\usepackage{subfiles}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{cor}{Corollary}[section]
\setlength{\parindent}{0cm}

\title{\textbf{My algebra notes}}
\author{Konrad Pisarczyk}
\begin{document}
\newcommand*\mean[1]{\overline{#1}}
\maketitle
\tableofcontents

\subfile{sections/motivation}

\section{Preliminaries}

\subsection{Set, function, binary operation}
We start with some definitions.


\begin{defn}[\textbf{Set}]
  A set is a collection of elements. \\
\textbf{TODO:} Imprecise, but this is it for now. See:
\begin{enumerate}
  \item \url{https://en.wikipedia.org/wiki/Set_(mathematics)}
  \item \url{https://en.wikipedia.org/wiki/Set_theory}
  \item \url{https://en.wikipedia.org/wiki/Zermelo-Fraenkel_set_theory}
\end{enumerate}
\end{defn}


\begin{defn}[\textbf{Cartesian product}]
  Let $A,B$ be sets. The cartesian product $A \times B$ is the set of all ordered pairs $(a,b)$ where $a \in A$ and $b \in B$:
  \[
    A \times B = \{ (a,b)\ |\ a \in A \text{\ and\ } b \in B \}
  \]
\end{defn}


\begin{defn}[\textbf{Function, mapping, map}]
  A \textbf{function} $f: X \rightarrow Y$ is a rule which assigns to \textbf{each} element of $X$ \textbf{exactly one} element of $Y$. \\
  Additionally, we name the following:
  \begin{itemize}
    \item The set $X$ is called the \textbf{domain} of the function
    \item The set $Y$ is called the \textbf{codomain} of the function
    \item If $x \in X$, the $f(x) \in Y$ is called \textbf{value of $f$ at $x$}, or the \textbf{image of $f$ at $x$}
    \item The set of values of all elements of $X$, denoted $f(X)$, is called \textbf{the image of $f$}
    \item If $A \subset X$, the \textbf{image of $A$ (under $f$)} is the set $f(A) = \{ f(a)\ |\ a \in A \} \subset Y$
    \item The \textbf{preimage (or inverse image) of $Y$} is the set $f^{-1}(Y) = \{ x \in X\ |\ f(x) \in Y \} \subset X$. \\
    \textbf{Warning:} We are not defining here or claiming anything about the inverse of $f$. In fact, the inverse of $f$ might not even exist
    \item If $B \subset Y$, the \textbf{preimage of $B$} is the set $f^{-1}(B) = \{x \in X | f(x) \in B \}$
  \end{itemize}
\end{defn}

Although short, some care is required with the above definition. For example, if not \textbf{every} $x \in X$ is mapped to some $y \in Y$, then $f$ is not a function. Also, two elements $x_1, x_2 \in X$, can map to the same $y \in Y$ (the definition allows it). Furthermore, we can describe functions as:
\begin{itemize}
  \item \textbf{injective} (or one-to-one):
    for $x_1,x_2 \in X$, \quad $f(x_1)=f(x_2) \Rightarrow x_1 = x_2$
  \item \textbf{surjective} (or onto):
    $f(X) = Y$, that is, every $y \in Y$ has a preimage in $X$
  \item \textbf{bijective} (or one-to-one \textbf{correspondence}):
    both injective and surjective, i.e. every $y \in Y$ has an \textbf{unique} preimage in $X$
\end{itemize}


% DEF: BINARY OPERATION
\begin{defn}[\textbf{Binary operation}]
  A binary operation on a set $S$ is a mapping from $S \times S$ to $S$:
  \[
    f:S \times S \rightarrow S
  \]
  \textbf{Note:} Such operation is \textbf{closed} (it maps $S \times S$ elements back to $S$).
\end{defn}












% GROUP THEORY -------------------------
\section{Group theory}
% DEF: GROUP
\begin{defn}[\textbf{Group}]
  A non-empty set $G$, equipped with a binary operation is a \textbf{group}
  if it satisfies the conditions:
  \begin{enumerate}
    \item \textbf{Associativity} \\
    $(ab)c = a(bc), \quad \forall a,b,c \in G$
    \item \textbf{Identity element} \\
    $\exists e \in G$ s.t. \quad $ea = ae = a, \quad \forall a \in G$
    \item \textbf{Inverse} \\
    $\forall a \in G$, $\exists a^{-1} \in G$ s.t. \quad $aa^{-1} = a^{-1}a = e$
  \end{enumerate}
\end{defn}
\textbf{Note:} We write a shorthand $ab$ to mean $a \bullet b$, where $\bullet: G\times G \rightarrow G$ is the binary operation in question. \\
\textbf{Note:} Closure is implied by the binary operation. \\





% COROLLARY: UNIQUE IDENTITY AND INVERSE
\begin{cor} \textbf{Uniqueness of identity and inverse} \\
Let $G$ be a group, and $a \in G$. Then, there is \textbf{only one inverse} of $a$ in $G$. Also, the identity \textbf{$e$ is unique} for the entire group.
\begin{proof}
Let $a \in G$ and suppose it has two distinct inverses, $a_1$ and $a_2$. Then we find that they must indeed be equal to each other:

\begin{align*}
a_1 &= a_1e     &&\text{(identity)} \\
      &= a_1(aa_2)  &&\text{($a_2$ is an inverse of $a$)} \\
      &= (a_1a)a_2  &&\text{(associativity)} \\
      &= ea_2     &&\text{($a_1$ is an inverse of $a$)} \\
      &= a_2      &&\text{(identity)}
\end{align*}
For the second claim, suppose that the identity is not unique and we have $e_1$ and $e_2$. But then a similar contradiction: $e_1 = e_1 e_2 = e_2$.
\end{proof}
\end{cor}


\begin{defn}[\textbf{Abelian group}]
  Let $G$ be a group. If $ab = ba \quad \forall a,b \in G$, then we say that the operation is \textbf{commutative}, and the group is \textbf{Abelian}.
\end{defn}

Note that in our definition of a group, although the group is not necessarily Abelian, the identity and inverses already behave in a commutative way.
We can relax or tighten the group axioms to create other group-like algebraic structures such as magma, groupoid, loop, monoid etc. See \url{https://en.wikipedia.org/wiki/Outline_of_algebraic_structures#Types_of_algebraic_structures}. Of these, here we only define the \textbf{monoid}, as we'll need it for defining a \textbf{ring}.

\begin{defn}[\textbf{Monoid}]
  We say that $M$, is a \textbf{monoid} if it satisfies the below:
  \begin{enumerate}
    \item \textbf{Associativity} \\
    $(ab)c = a(bc), \quad \forall a,b,c \in M$
    \item \textbf{Identity element} \\
    $\exists e \in M$ s.t. \quad $ea = ae = a, \quad \forall a \in M$
  \end{enumerate}
\textbf{Notes:}

\begin{itemize}
  \item The definition is similar to that of a group, but we no longer require for every element to have an inverse. \\
  \item Again, we're using shorthand $ab$ to mean $a \bullet b$.
  \item Again, the closure is implied by the binary operation.
\end{itemize}
\end{defn}


%%%%% SUBSECTION: HOMOMORPHISM
\subsection{Homomorphism}
Recall the earlier definitions of funcion/map/mapping. Mappings betwen algebraic structures of the same kind which preserve the operation (operations), are called homomorphisms (e.g.group homomorphism, ring homomorphism).

\begin{defn}[\textbf{Homomorphism}]
Let $(A,\bullet)$ and $(B, \star$) be two sets equipped with an operation (e.g. binary). A \textbf{homomorphism} is a map $f:A\rightarrow B$ such that:
\begin{equation*}
  f(x \bullet y) = f(x) \star f(y)
\end{equation*}
or in short:
\begin{equation*}
  f(xy) = f(x)f(y)
\end{equation*}

\textbf{Note:} This definition is in terms of a \textbf{binary} operation, but homomorphism definition is more general - consider 0-ary, 1-ary, 2-ary etc. See Wikipedia for precise definition. \\
\textbf{Note:} A ring homomorphism, for example, preserves the ring addition, and ring multiplication \\
\textbf{Note:} A homomorphism also preserves identity (TODO: Make this formal. I get why, but I don't get when wiki says that identity is 0-ary operation) <- this depends on the underlying structure, requires existence of identity, and cancellation property.
\end{defn}

\begin{thm}
Let $f$ be a homomorphism between two sets $A$ and $B$, where $i_a$ and $i_b$ are the identity elements of $A$ and $B$, respectively. Then $f(i_a) = i_b$, i.e. the identity is preserved. TODO: Make this about groups, or rings, or remove if not needed.
\end{thm}

\begin{defn}[\textbf{Group homomorphism}]
Let $A$ and $B$ be two groups. The map $f:A \rightarrow B$ if $\forall x,y \in A$,

\begin{equation*}
  f(xy) = f(x) f(y)
\end{equation*}

\textbf{Note:} The $f$ is a map, so all $x \in A$ are mapped, but we say nothing about whether it is injective or surjective.
\end{defn}





% RING THEORY -------------------------
\section{Ring theory}
\begin{defn}[\textbf{Ring}]
  We say that $(R, +, \bullet)$, a set $R$ equipped with two binary operations: $+: R \times R \rightarrow R$ (addition) and $\bullet: R \times R \rightarrow R$ (multiplication), is a \textbf{ring} if it satisfies the below:
  \begin{enumerate}
    \item $(R, +)$ is an \textbf{abelian group} \\
	(commutative, associative, identity, inverse)
    \item $(R, \bullet)$ is a \textbf{monoid} \\
	(associative, identity)
    \item \textbf{Multiplication is distributive w.r.t. addition}, i.e. $\forall a,b,c \in R$, we have:
      \begin{itemize}
        \item $a(b+c) = ab + ac$
        \item $(b+c)a = ba + ca$
      \end{itemize}
  \end{enumerate}
  \textbf{Note:} If additionally $(R, \bullet)$ is a commutative monoid, i.e. the multiplication is commutative, then we call the $(R, +, \bullet)$ a \textbf{commutative ring}.
\end{defn}

We are now ready to redefine the earlier \textbf{integral domain} in terms or rings. Let $R$ be a \textbf{commutative ring} as defined above. Now, recall the nine postulates for an integral domain:
\begin{itemize}
  \item \textbf{Closure of addition and multiplication}: satisfied by the binary operation
  \item \textbf{Uniqueness:} Not sure how to show this???
  \item \textbf{Commutative laws:} Since $R$ is a commutative ring, the $(R, +)$ is an abelian group and if we take $(R, \bullet)$ to be a \textbf{commutative} monoid, so it's satisfied
  \item \textbf{Associative laws:} Both $+$ and $\bullet$ operations satisfy these (both monoid and group satisfy the associativity)
  \item \textbf{Distributive law:} Satisfied through the definition of the ring
  \item \textbf{Additive identity (zero):} Since $(R, +)$ is a group, it has an additive identity
  \item \textbf{Multiplicative identity (unity):} Since $(R, \bullet)$ is a monoid, it has an identity element
  \item \textbf{Additive inverse:} Since $(R,+)$ is a group, it satisfies the inverse condition
  \item \textbf{Cancellation law:} This is new
\end{itemize}


So we can define the integral domain in terms of rings and monoids.

% DEF: INTEGRAL DOMAIN
\begin{defn}[\textbf{Integral domain}]
  An \textbf{integral domain} is a commutative ring $R$ which satisfies the \textbf{cancellation law}:
  \begin{equation*}
    a,b,c \in R \quad
    \text{and} \quad c \neq 0 \quad
    \text{and} \quad ca = cb \quad
    \Rightarrow \quad a=b
  \end{equation*}
\end{defn}

Examples:
\begin{itemize}
  \item Set of all integers
  \item Set of all rational numbers
  \item Set of all real numbers
  \item Set $J[\sqrt{3}]$ of all numbers of the form $a + b\sqrt{3}$ where $a$ and $b$ are ordinary integers, and $a+b\sqrt{3} = c + d\sqrt{3}$ if and only if $a=c, b=d$
\end{itemize}




\end{document}
