\documentclass[11pt]{article}
\usepackage{amssymb, amsthm, amsmath, enumerate}
\usepackage[a4paper, margin=0.5cm]{geometry}
\theoremstyle{definition}
\newtheorem{defn}{Definition}[section]
\newtheorem{thm}{Theorem}[section]
\newtheorem{exercise}{Exercise}[section]
\newtheorem{remark}{Remark}[section]

\title{\textbf{Introduction to Mathematical Analysis (exercises)}}
\author{Konrad Pisarczyk}
\begin{document}
\maketitle
\newcommand*\mean[1]{\overline{#1}}

These are the solutions to the book by C.R.J. Clapham.

\section{Axioms for the Real Numbers}
\textbf{Topics:} Fields, Order, Completeness, Upper bound, The Archimedean property

\begin{exercise}
Show that the set of real numbers of the form $a + b \sqrt{2}$, where $a$ and $b$ are rational, with the natural definition of addition and multiplication, is a field.
\end{exercise}
\begin{proof}
Recall (shorthand) the definition of field: a set $F$ closed under   two operations (addition, multiplication), with properties:
\begin{itemize}
  \item Addition: associative, commutative, $\exists 0$, $\exists{-a}$
  \item Multiplication: associative, commutative, $\exists{1}$, $  \exists{a^{-1}}$
  \item $a(b+c) = ab + ac$ ($*$ distributes over $+$:)
  \end{itemize}
First, set
  \[ F = \{ a + b \sqrt{2} : a,b \in \mathbb{Q} \} \]
We first show that the set $F$ is itself closed under both addition,   and multiplication. \\
Suppose $x,y \in F$, i.e., for some $i,j,k,l \in \mathbb{Q}$:
  \begin{align*}
    x &= i + j \sqrt{2} \\
    y &= k + l \sqrt{2}
  \end{align*}
  Then,
  \begin{align*}
    x + y &= i + j \sqrt{2} + k + l \sqrt{2} \\
          &= (i+k) + (j+l)\sqrt{2}
  \end{align*}
but $i+k \in \mathbb{Q}$ and $j+l \in \mathbb{Q}$ because we assumed the addition is `natural' (associativity, commutativity), and  so $x + y \in F$, i.e. \textbf{F} is closed under addition. \\
Similarly
  \begin{align*}
    x*y &= (i + j \sqrt{2}) * (k + l \sqrt{2}) \\
        &= ik + il\sqrt{2} + jk\sqrt{2} + 2jl \\
        &= ik + 2jl + (il + jk)\sqrt{2}
  \end{align*}
but $ik+2jl \in \mathbb{Q}$ and $il+jk \in \mathbb{Q}$, and so \textbf{F} is closed under multiplication. \\
To show that \textbf{addition is associative}, for $x,y,z \in F$, observe that, similarly, with $z = m + n\sqrt{2} \in F$, we have
  \begin{align*}
  x + (y + z) &= i + j\sqrt{2} + (k + l\sqrt{2} + m + n\sqrt{2}) \\
              &= (i + j\sqrt{2} + k + l\sqrt{2}) + m + n\sqrt{2} \\
    &= (x + y) + z
  \end{align*}
We see that the \textbf{addition is commutative}, because
  \begin{align*}
  x + y &= i + j\sqrt{2} + k + l\sqrt{2} \\
        &= k + l\sqrt{2} + i + j\sqrt{2} \\
        &= y + x
  \end{align*}
The \textbf{additive zero element} is the usual $0 = 0 + 0\sqrt{2}$ and it is clear that $0 + x = x,\forall x \in F$.
Now, pick any $x \in F$. Then $x = i + j\sqrt{2}$ for some $i,j \in \mathbb{Q}$. Then there is an \textbf{negative} of $x$, $-x = -i - j\sqrt{2} \in F$. \\
Associativity and commutativity of multiplication can be proved similarly. There also is a multiplicative identity, the usual $1 = 1 + 0\sqrt{2}$.
Also, for any non-zero $x = i + j\sqrt{2} \in F$, we have $x^{-1} := \frac{1}{x} = \frac{1}{i + j\sqrt{2}}$ for which $xx^{-1} = 1$. \\
Finally, the \textbf{multiplication distributes over addition}, for all $x,y,z \in F$ defined as before, we have
  \begin{align*}
  x(y + z) &= (i + j\sqrt{2})(k + l\sqrt{2} + m + n\sqrt{2}) \\
           &= ik + il\sqrt{2} + im + in\sqrt{2} \\
           &+ jk\sqrt{2} + 2jl + jm\sqrt{2} + 2jn \\
           &= ik + il\sqrt{2} + jk\sqrt{2} + 2jl \\
           &+ im + in\sqrt{2} + jm\sqrt{2} + 2jn \\
           &= (i + j\sqrt{2})(k + l\sqrt{2})
            + (i + j\sqrt{2})(m + n\sqrt{2}) \\
           &= xy + yz
  \end{align*}
Hence, $F$ is a field.
\end{proof}

\begin{exercise}
Prove that the following rules hold in any field, justifying each step by a postulate, a result established in the text or an earlier exercise:
  \begin{enumerate}[(i)]
    \item If $a + b = a + c$, then $b = c$.
    \item If $a + b = a$, then $b = 0$.
    \item $-(a + b) = (-a) + (-b)$.
    \item $-(a-b) = b-a$.
    \item If $ab = a$, then $b=1$.
    \item $(ab^{-1})^{-1} =  ba^{-1}$.
    \item $a^{-1}(bc^{-1}) = b(ac)^{-1}$.
  \end{enumerate}
\end{exercise}
\begin{proof}
\begin{enumerate}[(i)]
\item Suppose $a + b = a + c$. Then
  \begin{align*}
    b &= b + 0        &\text{by 3 (defn. of zero element)} \\
      &= b + a + (-a) &\text{by 4 (defn. of negative)} \\
      &= b + a - a    &\text{by Theorem 1} \\
      &= a + b - a    &\text{by 2 (addition commutative)} \\
      &= a + c - a    &\text{by assumption} \\
      &= a - a + c    &\text{by 2 (addition commutative)} \\
      &= c            &\text{by 4 (defn. of negative)} \\
  \end{align*}
\item Suppose $a + b = a$. Then
  \begin{align*}
    b &= b + 0        &\text{by 3 (defn. of zero)} \\
      &= b + a + (-a) &\text{by 4 (defn. of negative)} \\
      &= a + b + (-a) &\text{by 2 (addition commutative)} \\
      &= a + (-a)     &\text{by assumption} \\
      &= 0            &\text{by 4 (defn. of negative)}
  \end{align*}
\item
  \begin{align*}
    -(a + b) &= (-1)(a + b)   &\text{by Thm.2(vi)} \\
             &= (-1)a + (-1)b &\text{by 9 (mult. distrib. over add.)} \\
             &= (-a) + (-b) &\text{by Thm.2(vi)}
  \end{align*}
\item
  \begin{align*}
    -(a - b) &= (-1)(a - b)       &\text{by Thm.2(vi)} \\
             &= (-1)(a + (-1)b)   &\text{by Thm.1)} \\
             &= (-1)a + (-1)(-1)b &\text{by 9 (mult. distrib. over add.)} \\
             &= (-1)(-1)b + (-1)a &\text{by 2 (addition commutative)} \\
             &= (-1)(-1)b - a     &\text{by Thm.2(vi)} \\
             &= (-1)(-b) - a      &\text{by Thm.2(vi)} \\
             &= -(-b) - a         &\text{by Thm.2(vi)} \\
             &= b - a             &\text{by Thm.2(i)}
  \end{align*}
\item Suppose $ab = a$. Then $ab = a1$ by definition of \textbf{identity element 1}. Then, by Thm.3(vi) (cancellation law), either $a = 0$ or $b = 1$. If $a = 0$ then there is nothing to prove, so $b = 1$.
\item Consider first the case when $a \neq 0$ and $b^{-1} \neq 0$ and $b \neq 0$. Then
  \begin{align*}
    (ab^{-1})^{-1} &= (b^{-1})^{-1}a^{-1} &\text{by Thm.3(vii)} \\
                   &= ba^{-1}             &\text{by Thm.3(i)} \\
  \end{align*}
  If either $a = 0$ or $b^{-1} = 0$ or $b = 0$, the assertion holds clearly, as both sides of the equation will equal zero as multiplication associative and commutative, and Theorem 2(iii), and by note below on trivial field.
\item Note that the field multiplication is both commutative and associative, and so in the first step below, we omit the brackets without any ambiguity. Suppose also that $a \neq 0$ and $b \neq 0$. Then
  \begin{align*}
    a^{-1}(bc^{-1}) &= a^{-1}bc^{-1} &\text{by the remark} \\
                    &= bc^{-1}a^{-1} &\text{by 6 (mult. commutative)} \\
                    &= b(ac)^{-1}           &\text{by Thm.3(vii)}
  \end{align*}
The remaining case is if $a = 0$ or $b = 0$. If the former, then by the note below and Therem 2 iii, both sides of the equation equal zero, and so it holds vacuously.. If the latter, then we note that multiplication is commutative and associative (field axioms), and that multiplying any element by zero yields zero (Theorem 2(iii)), from which it quickly follows that both sides of the equation equal zero.
\end{enumerate}
\end{proof}
\begin{remark} \textbf{Inverse of zero and the trivial field.} If $a^{-1} = 0$ for any $a \in F$, then $x = 0 \forall x \in F$, for suppose $a^{-1} = 0$ for some $a in F$. Then $1 = aa^{-1} = a0 = 0$ by Theorem 2(iii), and also, for any $x \in F$, we have $x = x1 = x0 = 0$, i.e. the ring contains a single element, whether called $0$ or $1$.
\end{remark}


\begin{exercise}
Show that, in any field, the only elements satisfying $x^2 = x$ are $0$ and $1$.
\end{exercise}
\begin{proof}
Suppose that $F$ is a field, and pick $x \in F$ such that $x^2 = x$. Remember that $x^2 = xx$ by definition, and so $xx = x = x1$. Then, by Theorem 3 (vi) (cancellation law), either $x = 0$ or $x = 1$. Now, both $x = 0$ and $x = 1$ satisfy the equation because $0.0 = 0$ and $1.1 = 1$.
\end{proof}
\begin{proof}
\textbf{Incorrect proof.} \\
Suppose that $F$ is a field, and pick $x \in F$ such that $x^2 = x$. Suppose also that $x \neq 0$. Then $xx = x$ has the unique solution $x = \frac{x}{x}$ by Thm.3(iii). Then $x = xx^{-1} = 1$ by Thm.3(iv) and definition of inverse. If, conversely, $x \neq 1$, then taking $xx = x$, and multiplying on both sides by $x^{-1}$, we have $xxx^{-1} = xx^{-1}$, so $x = 1$. $\leftarrow$ This is wrong. We don't know if $x$ has an inverse.
\end{proof}


\begin{remark}
\label{rem-ltpositive}
In any ordered field, $a < b$ if and only if $b - a$ i positive.
\end{remark}


\begin{exercise}
Prove that the following rules hold in any ordered field:
  \begin{enumerate}[(i)]
    \item $a - b < a - c$ if and only if $b > c$.
    \item If $a < b$ and $c < 0$, then $ac > bc$.
    \item If $a < b$, then $-a > -b$.
    \item If $a > 0$ and $b > 0$, $a > b$ if and only if $a^2 > b^2$.
  \end{enumerate}
\end{exercise}
\begin{proof}
\begin{enumerate}[(i)]
\item
We need to prove both directions. \\
\begin{align*}
              & b > c \\
  \Leftrightarrow & b - c > 0         &\text{by Remark \ref{rem-ltpositive}} \\
  \Rightarrow     & 0 + a < b - c + a &\text{by (O1), added $a$} \\
  \Rightarrow     & a + (-b) < b - c + a + (-b) &\text{by (O1), added $-b$} \\
  \Rightarrow     & a - b < a - c \\
\end{align*}
Conversely,
\begin{align*}
              & a - b < a - c \\
  \Rightarrow & a - b + b < a - c + b &\text{by (O1), added $b$} \\
  \Rightarrow & a < a - c + b \\
  \Rightarrow & 0 < - c + b           &\text{by (O1), added $-a$} \\
  \Rightarrow & c < b                 &\text{by (O1), added $c$}
\end{align*}
\item
Now, since $c < 0$, then adding $-c$ on both sides (by O1), we have $c + (-c) < 0 + (-c)$, so $0 < -c$. So we have $a < b$ and $0 < -c$. Then, multiplying by $-c$ (by O2), $-ac < -bc$, which is equivalent (by Remark \ref{rem-ltpositive}) to $-bc - (-ac) > 0$. As addition is commutative, and by Thm2(i), this implies that $ac - bc > 0$. Adding $bc$ to both sides (O1), we conclude that $ac > bc$, as required.
\item
Suppose $a<b$. Also, $-1 < 0$, and so by Exercise 1.4(ii) (we just proved it), taking $c = -1$, we have $a(-1) > b(-1)$, i.e. $-a > -b$, as required.
\item
We need to prove both directions. \\
Suppose $a > 0$ and $b > 0$. $a > b$. Now, since $a > b$, multiplying by $a$ (O2), $a^2 > ba$. Similarly, multiplying by $b$ (O2 again), $ab > b^2$. Now, by (O4), $a^2 > ba > b^2$, i.e. $a^2 > b^2$, as required. \\
Conversely, suppose $a^2 > b^2$, and $a>0$, $b>0$. \\
TODO: FINISH THIS
\end{enumerate}
\end{proof}

\begin{exercise}
Provide the proofs for Theorem 5(i) to (v), i.e. below. \\
In an ordered field
\begin{enumerate}[(i)]
\item If $a$ is positive, $(-a)$ is negative,
\item $a$ is negative if and only if $a<0$,
\item the sum of two negative elements is negative,
\item the product of two negative numbers is positive,
\item the product of a negative element and a positive element is negative.
\end{enumerate}
\end{exercise}

\begin{proof}
\begin{enumerate}[(i)]
\item We need to show that $(-a)$ is negative, i.e. that $-(-a)$ is positive. Now, $-(-a) = a$ by Thm.2(i). But, as assumed, $a>0$, i.e. $a$ is positive, as required.
\item We prove both ways. \\
First, suppose $a$ is negative. Then, by definition, $(-a)$ is positive, i.e. $-a>0$. Then, by (O2), adding $a$ on both sides, $-a + a > 0 + a$, so $0>a$, as required. $\leftarrow$ Note that this is not required, as below proof works both ways. \\
Conversely, suppose $a<0$. By Remark \ref{rem-ltpositive} with $b=0$ this means that $a<0$ if and only if $0-a$ is positive, i.e. $-a$ is positive. Thus $a$ is negative, by definition.
\item Suppose $a$ and $b$ are negative. Then, by definition, $(-a)$ and $(-b)$ are positive. But, by (P1), the sum of two positive elements is positive, and so $-a + (-b) = -(a+b)$ is positive. By definition, this means that $a+b$ is negative, as required.
\item Suppose $a$ and $b$ are negative ,i.e. $-a$ and $-b$ are positive. But, by (P2), their product, $(-a)(-b)$ is positive, but also equal to $ab$, by Thm.2(v). Hence $ab$ is positive, as requried.
\item Suppose $a$ is negative, and $b$ is positive. Then, by definition, $-a$ is positive. Then, by Thm.2(iv), $(-a)b = -(ab)$, which is a product of two positive numbers, and so by (P2), their product is itself positive. But as $-(ab)$ is positive, then by definition $ab$ is negative, as required.
\end{enumerate}
\end{proof}


\begin{exercise}
Suppose that $F$ is a field in which certain elements are called positive such that \textbf{P1}, \textbf{P2} and \textbf{P3} hold. Define the relation $<$ by $a<b$ if and only if $b-a$ is positive. Show that $<$ satisfies \textbf{O1}, \textbf{O2}, \textbf{O3} and \textbf{O4}, and hence that $F$ is ordered.
\end{exercise}
\begin{proof}
\begin{enumerate}[(i)]
\item To show O1 holds, suppose that $a < b$. We want to show $a+b < b+c$. Consider $b + c - (a + c) = b - a$. But we know $b-a$ is positive, and so $b + c - (a+c)$ is also positive, i.e. $a+c < b+c$, as required.
\item To show O2 holds, suppose $a<b$ and $0<c$, i.e. by definition, $b-a$ is positive and $c-0 = c$ is positive. Now, by P2, the product of the two is also positive, i.e. $(b-a)c = bc - ac$ is positive, meaning that $ac < bc$.
\item ??? To show that O3 holds, we need to show that for any $a$ and $b$ we have that only one holds: $a<b$, $a=b$, $b<a$. But by P3, only one is true: $a$ is positive, $a=0$ or $(-a)$ is positive, similarly for element $b$. Now, consider $a+b \in F$. Then, by P3, either $a+b$ is positive, $a+b=0$, or $-(a+b)$ is positive. In the first case???? In the second case, $a + b + (-b) = -b$, so $a = -b$
\item To show that O4 holds, suppose that $a<b$ and $b<c$. Then, by definition, $b-a$ and $c-b$ are both positive. We wish to show that $c-a$ is positive. Now, by P1, the sum of any two positive elements is positive, and so $b-a + c-b = c-a$ must also be positive, i.e. $a<c$, as required.
\end{enumerate}
\end{proof}

\begin{remark}
In any ordered field $F$, for any $a,b \in F$, it holds that
\[
  |a-b| = |b-a|
\]
\begin{proof}
In first case, if $a-b \ge 0$, then $|a-b| = a-b$. But as $a-b \ge 0$, then $a \ge b$, and so $b-a \le 0$. Suppose also that $b-a \neq 0$, because otherwise $a = b$, and the conclusion is immediate. Then $b-a < 0$, and so $|b-a| = -(b-a) = a-b$, and so $|a-b| = |b-a|$.
\end{proof}
\end{remark}

\begin{exercise}
Prove that, in an ordered field, if $r>0$, then $|x-a| < r$ if and only if $a-r < x < a+r$.
\end{exercise}
\begin{proof}
Suppose that $r>0$. We prove both ways. \\
Suppose $|x-a|<r$. If $x-a \ge 0$, then, by definition $|x-a| = x-a$, and so $x-a<r$, i.e. $x < a+r$. In the other case, when $x-a < 0$, then $|x-a| = -(x-a)$, and so $a-x<r$, i.e. $x > a-r$. UNFINISHED??? Need both ways.
\end{proof}


\section{Sequences}
\textbf{Topics:} Limits of a sequence, Sequences without limits, Monotone sequences
\section{Series}
\textbf{Topics:} Infinite series, Convergence, Tests, Absolute convergence, Power series
\section{Continuous functions}
\textbf{Topics:} Limit of a function, Continuity, The intermediate value property, Bounds of a continuous function
\section{Differentiable functions}
\textbf{Topics:} Derivatives, Rolle's theorem, The mean value theorem
\section{The Riemann integral}
\textbf{Topics:} Introduction, Upper and lower sums, Riemann-integrable functions, A necessary and sufficient condition, Monotone functions, Uniform continuity, Integrability of continuous functions, Properties of the Riemann integral, The mean value theorem, Integration and differentiation
\end{document}
