# Life in the UK test, notes

Limit: 45 min

Christian communities:
  - 3-4 century
House of commons: (MPs)
  - Westminster
  - chair: Speaker
Cenotaph: war memorial (I war), Whitehall, design Sir Edwin Lutyens
Lutyens: New Delhi
Thatcher: first female PM

Jury:
  - age 18-75
  - random, electoral register
Solicitors' charges:
  - based on how much time
By law, media have to give a balanced coverage
  of all political parties, equal time to rival
  viewpoints before election:
  - TV
  - radio
United Kingdom of Great Britain and Northern Ireland
Crown dependency + part of UK:
  - England
  - Commonwealth: 53 states
  - Great Britain: England, Scotland, Wales
  - Ireland: Northern, Southern
    - St Patrick, patron Saint of Ireland
  - Scotland (flag: white cross, blue background)
    - St Andrews: home of golf
    - Robert Burns 'The Bard' poet
    - Loch Lomond, the Trossachs National Park: West of Scotland
    - New Year's Eve: Hogmanay
    - Fringe comedy festival (Edinburgh)
    - SECC: Scottish Exhibition and Conference Centre in Glasgow
  - Wales
    - Snowdonia
    - Bodnant Garden
    - St. David day: 1st of March, patron saint of Wales
Crown dependency but not part of UK:
  - Channel Islands
  - Isle of Man
Overseas terirories:
  - Falkland Islands
  - St Helena

Parliament
  - Welsh Assembly (1999-) 60 AM members
  - Scottish Parliament 108 members


--------------

## Ideology, principles
- Individual liberty: fundamental principle

## Demography
- British Empire population 400 million
- 84% British population is in England
- 10% of population has grandparent born outside of UK
- 1% Sikh population
- 21% no religion
UK population:
- England 84%
- Wales 5%
- Scotland 8%
- Northern Ireland <3%

| Year | Population |
| ---  | --- |
| 1600 | Just over 4 million |
| 1700 | 5 million |
| 1801 | 8 million |
| 1851 | 20 million |
| 1901 | 40 million |
| 1951 | 50 million |
| 1998 | 57 million |
| 2005 | <60 million |
| 2010 | >62 million |

## Art, culture
- Gothic medieval architecure popular again in 19th century
- Gilbert and Sullivan: 19th century comic operas: 'The Mikado', 'HMS Pinafore', 'The Pirates of Penzance'
- 1930s: Bitish Film studios flourished
- Theatreland: area in London with famous theatres
- Henry Purcell: English composer
- John Barbour wrote 'The Bruce' in Gaelic

## Geography
- Lake District: largest national park in England
- Australia coast mapped by James Cook
- Wales population is 5% of UK
- Scotland population is 8% of UK

## History
- 6000 years ago: farmers came from SE Europe
- Bronze age:
  - 4000 years ago
  - burials in round barrows
- Skara Brae: best preserved prehistoric village in N.Europe, Stone Age
- Cnut, first Danish king
- AD 43 Romans invade
- AD 410: Romans leave
- AD 600 first Anglo-Saxon kingdoms
- AD 789 Vikings attack
- Jutes and Anglo-Saxons invade
- Iron age: Celtic language
  - first coins
- Roman invasion: 400 years
### 1000
- Norman conquest
- 1066 Battle of Hastings, invasion by William the Conqueror (Duke of Normandy)
  Saxon king Harold defeated
- 1200 English rule Pale around Dublin
- 1215 Magna Carta
- 1284 Statute of Rhuddlan, King Edward I annex Wales to England
### 1300
  THEME: French,BlackDeath
- 14th century: last Welsh rebellions defeated
- 1314 Battle of Bannockburn: Robert the Bruce beats English in Scotland
- Middle Ages:
  - 1337 Hundred Years War starts (116 years)
    - English kings (Henry V) vs France
  - 3 houses (Estates) in Scotland: lords, commons, clergy
  - 1348 Black Death: 1/3 population died
  - Gentry: owners of large areas of land, appeared after Black Death
  - Engineers came from Germany
  - House of Lords: nobility, great landowners, bishops
  - 1400 English is official language
  - 1400 Canterbury Tales
  - 1415 Battle of Agincourt
  - 1450s Hundred Years War ends: French leave England
- 1485 Middle Ages/Medieval Period ends
- 1485 Battle of Bosworth Field: War of Roses won by Lancaster over York
### 1500
- Protestant ideas began to spread
- 1560 Scotland separates from Pope
- 1588 Spanish Armada defeat by English
  - Sir Francis Drake commander beat them, founder of naval tradition,
    - The Golden Hind: ship, sailed around the world
  - Queen Elizabeth I (protestant) became popular
### 1600
- 1605 5 November Bonfire Night
- 1606 first Union Flag created
- 1642 English Civil war
    Roundheads vs. Cavaliers
    Charles I try to arrest 5 parliamentary leaders
- 1649 Charles I executed
- 1656 First Jews in London
- 1665 Plague in London
- 1666 Fire in London
- 1679 Habeas Corpus: right to trial
- 1680 Huguenots: Refugees from France
- 1688 William of Orange invades England
- 1688 Glorious Revolution: began constitutional monarchy with William in Eng
- 1689 Bill of Rights: King less power, parliament more power
- 1690 Battle of Boyne: James II invades Ireland with French
- 1695 Free press: News operate without govt licence
### 1700
- 1707 The Act of Union: The Kingdom of Great Britain
- Enlightenment: 18th century
  - 1720 Huguenots stop arriving
  - 1721-1742 Sir Robert Walpole PM
  - 1745 Charles Edward Stuart (Bonnie Prince Charlie) raised an army
    with Scottish Highlands clansmen
  - 1746 Battle of Culloden: Clans lost to Chieftains
  - 1776 13 American colonies declared independence
  - 1783 Britain recognises American independence
  - 1789 French Revolution, war with England
### 1800
- Highland Clearances: Scottish leave for North America
- 1800 Act of Union: Created United Kingdom of GB and Ireland
- 1805 Battle of Trafalgar:
       Won against French and Spanish fleets
       Admiral Nelson dies
- 1807 Illegal to trade slaves on British ships
- 1815 Battle of Waterloo: Duke of Wellington defeated Napoleon
- 1830s-1840s Chartists
- 1833 Emancipation Act: slavery abolished
- 1837 Queen Victoria became queen
- 1840s Punch: satirical magazine
- 1846 Corn laws repealed: prevented import of cheap grain
- 1870-1914 influx of Russian/Polish Jews (120,000)
- 1895 National Trust founded: preserve buildings
- 1896 first film screened in UK
- 1899 Boer War begins
### 1900
- 1901 Queen Victoria ends reign
- 1902 Boer War ends
- 1902 motor car racing starts
- 1914 WWI
- 1916 Battle of Somme (60,000 casualties in a day)
- 1918 +30 women vote
- 1921 Emmeline Pankhurst suffragettes: Women right to vote 21, same as men
- 1921 South Ireland independence: Irish Free state
- 1922 BBC first broadcast
- 1922 Ireland splits
- 1928 Women vote at 21
- 1928 UK fully democratic
- 1930s Great Depression: shipbuilding badly affected
- 1932 First TV broadcast London->Glasgow, John Logie Baird
- 1935 A. Hitchcock: The 39 Steps
- 1936 BBC regular broadcasts
- 1944 Education Act: free secondary edu in Eng/Wales (R.A. Butler)
- 1945-1951 PM Clement Attlee
- 1947 9 colonies granted independence
- 1948 NHS: Aneurin Bevan
- 1949 Irish Free State becomes a republic
- 1950 Immigration encouraged to help economy
- 1953 EEC founded
- 1953 DNA discovered: London and Cambridge Unis
- 1954 'Belles of St Trinian's': Frank Launder
- 1957 March 25: EU founded, Treaty of Rome
- 1959 Margaret Thatcher joins parliament
- 1969 Women vote at 18
- 1969 the Troubles in NI: 3000 casualties
- 1973 UK joins EEC (European Economic Community)
- 1973 UK joins EU
- 1982 Argentina invades Falkland Islands
- 1998 Northern Ireland Assembly formed
- 1999 Scottish Parliament established, Welsh Assembly
### 2000
- 2009 leave Iraq
- 2010 Coalition government

- Henry VII: Reduced power of nobles
- CoE established by Henry VIII to divorce his first wife, Catherine of Aragon
  Henry VIII: King of Ireland
- American colonies wantes independence from Britain because of tax


## Law and Politics
- England was a republic for 11 years
- General Elections: held every 5 years
- MPs meet weekly
- 15 Jury members in Scotland
- 12 jury members in England, Wales, Northern Ireland
- MEPs: elected members of European Parliament
- Hansard: report containing everything said in Parliament
- Secretary of State: education, health, defence
- Children 10-17 in England, Wales, Northern Ireland: Youth Court. Serious cases: Crown Court
- Most serious cases: High court
- Life peers: appointed by the monarch on advice of PM
- Suspected extremist/terror activity: report to police
- Chartists: campaign for right to vote for working class
- PAYE: System that automatically deducts tax
- small claims procedure: < £10,000
England & Wales:
  - County Courts: personal injury, family matters,
    breaches of contract, divorce
  - Sir Robert Walpole: first English PM (1721-1742)
Wales, Scotland, Northern Ireland:
  - received more separate power in 1997
  - Scottish Parliament (1999-), meets at Holyrood
  - Northern Ireland Assembly: 108 members, suspended a few times

## Religion
- Edward VI: Book of Common Prayer
- Hannukah: eight days, Jewish, 8 candles on menorah
- Vaisakhi: 14 April Sikh
- Lent: 40 days before Easter
- Pancake day: the day before Lent
- King James Bible: new translation into English
- Patron saint days:
  - 1  March: St David’s Day, Wales
  - 17 March: St Patrick’s Day, Northern Ireland
  - 23 April: St George’s Day, England
  - 30 November: St Andrew’s Day, Scotland.

## Royal genesis
- Elizabeth I: younger daughter of Henry VIII
- Elizabeth II:
  - Opens parliamentary session each year
  - married to Prince Philip (Duke of Edinburgh)
  - 2012: diamond jubilee

## Science
- Concorde: world's only supersonic commercial airliner: Britain/France
- radar: Sir Robert Watson-Watt

## Buildings, places
- Big Ben (Elizabeth Tower): 150 years old
- Clifton Suspension Bridge: (Isambard Kingdom Brunel)
- Millenium Stadium: Cardiff
- St Paul Cathedral (Sir Christopher Wren)
- Tower of London (William the Conqueror), tour guides called Beefeaters
- Dumfries House, Scotland (Robert Adam)

## Random facts, uncategorised
- Anyone can complain about police by writing to Chief Constable of the police force involved
- Crimean War introduced The Victoria Cross medal
- English language: Norman French and Aglo Saxon
- Insulin invented by John MacLeod, Scottish
- HMS Victory: Portsmouth Admiral Nelson
- Bessemer process: mass production of steel
- Charles II hid in an oak tree to hide from Cromwell's army
- Historic Scotland: Looks after Edinburgh castle
- Bayeux Tapestry: 70m long, Battle of Hastings
- Laurence Olivier Awards in London
- Halloween 31st October
- NSPCC: National Society for the Prevention of Cruelty to Children 
- Tim Berners Lee: Invented WWW
- Viking language: scunthorpe, grimsby
- CoE, protestant church, aka:
  - Anglican Church
  - Episcopal Church (Scotland, USA)
- lent starts on Ash Wednesday, starts 40 days before Easter
- Free TV licence: Over 75 (50% discount for blind people)
- pubs open at 11am (12pm on Sundays)
- School governor roles:
  - monitor and evaluate school performance
  - set strategic direction of the school
- Crystal Palace: cast-iron/plate-glass Hyde Park, Great Exhibition (1851)
- What you will be given to gove before a GeneralElection takes place:
  - poll card
- Sir Frank Whittle:
  - Jet engine, 1930
- William Wordsworth: The Daffodils
- Lord Protector: Oliver Cromwell
- Florence Nightingale: born in Italy 1820

# Sport
Olympic:
  - Hosted in UK in 1908, 1948, 2012
  - Mo Farah: running: first Briton win gold 10,000 metres
  - Dame Kelly Holmes: running: two gold medals (2004)
  - Sir Steve Redgrave: rowing, five consecutive gold medals
- 1966 FIFA World Cup
  - hosted in England
  - England:West Germany (4:2)
  - captain Bobby Moore
- 1954 Sir Roger Bannister: first man in the world to
       run 1 mile in < 4 minutes
- 2012 Sir Bradley Wiggins won Tour de France

## Other, heraldry, symbols
- Rose: associated with England
- Union Flag, three crosses:
  - St George, England, red cross on white
  - St Andrew, Scotland, white cross on blue
  - St Patrick, Ireland, red cross on white
- Diwali 5 days
- Hannukah 8 days
